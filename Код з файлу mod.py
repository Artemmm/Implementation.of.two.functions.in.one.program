def function2(n):

    y = 1
    for i in range(1, n + 1):
        y = y * (2 * i - 1)

    return y
