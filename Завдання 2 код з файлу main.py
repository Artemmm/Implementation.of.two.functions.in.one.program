import math
from mod import function2
def expression (alpha):
    z = math.cos(alpha) + math.cos(2 * alpha) + math.cos(6 * alpha) + math.cos(7 * alpha)

    return z

alpha = float(input("Введіть від якого числа (х) знаходити синус: "))
print(" Значення виразу z = ", expression(alpha), "при alpha = ", alpha)
n = 3
print(" \n Дано натуральне число n = ", n)
print(" Функція y = 1 * 3 * 5 *...(2n - 1)")
print(" Тоді y = ", function2(n))
